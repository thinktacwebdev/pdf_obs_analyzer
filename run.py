import os
from pprint import pprint

import PyPDF2 as pypdf
import requests

import firebase_service

tempPath = "tmp"


def download_file_from_google_drive(id, destination, f_token):
    if f_token:
        URL = f'https://www.googleapis.com/drive/v3/files/{id}?alt=media'
    else:
        URL = id
    response = requests.get(URL, headers={'Authorization': f'Bearer {f_token}'})
    open(destination, 'wb').write(response.content)


def analyze(destination):
    counter = 0
    firebase_data = dict()
    pdfobject = open(destination, 'rb')
    pdf = pypdf.PdfFileReader(pdfobject)
    data = pdf.getFormTextFields()
    firebase_data['Observation Data'] = data
    firebase_data['Total # cells in obs sheet'] = len(data)

    for k, v in data.items():
        if data[k] == None:
            counter += 1
    firebase_data['Total cells filled in obs sheet'] = len(data) - counter
    return firebase_data


def mainfunction(reqData):
    destination = os.path.join(tempPath, reqData["file_name"])

    try:
        download_file_from_google_drive(reqData['file_id'], destination, reqData['f_token'])
        fb_data = analyze(destination)
    except Exception as e:
        print('error', e)
        fb_data = {'Observation Submitted': True}
    del reqData['f_token']
    fb_resp = firebase_service.write_to_db({**fb_data, **reqData})
    return fb_resp


if __name__ == "__main__":
    file_id = 'https://firebasestorage.googleapis.com/v0/b/testapp-bb252.appspot.com/o/UsersActivity%2Fjeevan.hs%40thinktac.com%2FSamarth%2FCQ05%2FAssignment%20-%20Observation?alt=media&token=2ba84108-dc0c-4068-859e-c63854020594'
    file_name = 'Evaporative Cooling observation sheet.pdf'
    f_token = ''
    destination = os.path.join(tempPath, file_name)
    try:
        # download_file_from_google_drive(file_id, destination, f_token)
        obj = {
            'student_name': 'Jeevan',
            'student_email': 'jeevan.hs@thinktac.com',
            'JDocID': 'PswKySIicwClHcG8Q6QQ',
            'file_id': file_id,
            'file_name': file_name,
            'tacCode': 'BA01',
            'TAC Name': 'BookWidgets',
            'f_token': ''
        }
        data = mainfunction(obj)
    except():
        print('error')
    fb_data = analyze(destination)
    pprint(fb_data)
