from datetime import datetime

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from pprint import pprint

# Use a service account
cred = credentials.Certificate('serviceAccount.json')
firebase_admin.initialize_app(cred)

db = firestore.client()


def read_tac_master():
    d = db.collection('Tac').document('TAC-Master').get()
    return d.to_dict()['TACList']


def write_to_db(child_data, field_to_check='ChildEmail'):
    stream_empty = True
    docs = db.collection_group('Children').where('child_name', u'==', child_data['student_name']) \
        .where(field_to_check, u'==', child_data['student_email']).stream()
    # child_data['Submission Date'] = convert_date(child_data['submit_date'])
    child_data['addedOn'] = datetime.now()

    data = {
        'ChildName': child_data['student_name'],
        'ChildEmail': child_data['student_email'],
        'Registered email': 'unknown!',
        'Child School': 'unknown!',
        'Child School Grade': 'unknown!',
        'Child School Section': 'unknown!',
        'School Board': 'unknown!'
    }

    for doc in docs:
        stream_empty = False
        data['Child School'] = doc.to_dict()['child_details']['school']
        data['School Board'] = doc.to_dict()['child_details']['board']
        data['Child School Grade'] = doc.to_dict()['child_details']['classstd']
        data['parent_email'] = doc.to_dict()['parent_email']
    if stream_empty:
        if field_to_check == 'ChildEmail':
            return write_to_db(child_data, 'parent_email')
        # print(f'Doc not found for {child_data["student_name"]},{field_to_check} = {child_data["student_email"]}')
    path = updateEvalDocs(child_data, data)
    writeToJourney(child_data)
    return path


def writeToJourney(child_data):
    path = f'Journey/{child_data["JDocID"]}'
    db.document(path).collection('Details').document('CourseData').set(
        {'TAC Data': {child_data['tacCode']: child_data}},
        merge=True)


def updateEvalDocs(child_data, data):
    e_stream_empty = True
    eval_docs = db.collection('Evaluation').where('ChildName', u'==', child_data['student_name']) \
        .where('ChildEmail', u'==', child_data['student_email']) \
        .where(u'TAC Name', u'==', child_data['TAC Name']).stream()
    del child_data['student_email']
    del child_data['student_name']
    path = ''
    for e_doc in eval_docs:
        e_stream_empty = False
        path = e_doc.reference.path
        db.document(path).set(child_data, merge=True)
    if path != '':
        return path

    if e_stream_empty:
        newDoc = db.collection('Evaluation').add({**child_data, **data})
        return newDoc[1].path


def convert_date(date):
    date_time_str = date
    date_time_obj = datetime.strptime(date_time_str, '%a, %d %b %Y %I:%M:%S %z')
    return datetime.fromtimestamp(datetime.timestamp(date_time_obj))


def searchTAC(TACList, tc):
    for tac in TACList:
        if tac['TAC_Code'] == tc:
            return tac['displayName']


if __name__ == "__main__":
    read_tac_master()
