import ast
import run
import ast

import run

tempPath = "tmp"


def cors_enabled_function(request):
    if request.method == 'OPTIONS':
        headers = {
            'Access-Control-Allow-Origin': "*",
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600'
        }

        return ('', 204, headers)

    headers = {
        'Access-Control-Allow-Origin': "*"
    }

    return (analyze(request), 200, headers)


def analyze(request):
    if request.data:

        byte_str = request.data
        dict_str = byte_str.decode("UTF-8")
        reqData = ast.literal_eval(dict_str)
        resData = run.mainfunction(reqData)
        return {'path': resData}
    else:
        return {'path': ''}
